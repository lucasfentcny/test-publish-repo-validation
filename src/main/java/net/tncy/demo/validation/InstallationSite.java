/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.tncy.demo.validation;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.*;

/**
 *
 * @author x-roy
 */
public class InstallationSite {

    @NotNull
    @Size(min = 1)
    private String name;

    @Size(min = 1)
    private String address;

    @Size(min = 1)
    private String postCode;

    @Size(min = 1)
    private String city;

    @Size(min = 2, max = 2)
    private String country;

    @Pattern(regexp = "\\d+")
    private String phoneNumber;

    @Max(90)
    @Min(-90)
    private Float latitude;

    @Max(180)
    @Min(-180)
    private Float longitude;

    public InstallationSite() {
    }

    public InstallationSite(String name) {
        this.name = name;
    }

    public InstallationSite(String name, String address, String postCode, String city, String country, String phoneNumber, Float latitude, Float longitude) {
        this(name);
        this.address = address;
        this.postCode = postCode;
        this.city = city;
        this.country = country;
        this.phoneNumber = phoneNumber;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

}
